import zmq
import time
import sys

def order_placing_service(port):
    print("Starting Order Placing Service on port: %s" % port)
    context = zmq.Context()
    socket = context.socket(zmq.PULL)
    socket.bind("tcp://*:%s" % port)

    for reqnum in range(10):
        message = socket.recv()
        print(">- Order Placed: %s" % message)
