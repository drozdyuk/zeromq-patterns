import zmq
import time
import sys

def authenticator(port="5556", next_filter_port="5557"):
    """Looks at the message and removes '(security)' string from it"""

    context = zmq.Context()
    socket = context.socket(zmq.PULL)
    socket.bind("tcp://*:%s" % port)

    next_filter = context.socket(zmq.PUSH)
    next_filter.connect("tcp://127.0.0.1:%s" % next_filter_port)

    for reqnum in range(15):
        # Wait for next request from client
        message = socket.recv()
        secure = message.replace("(security)","")
        print("-- authenticator: %s" % secure)
        next_filter.send(secure)
