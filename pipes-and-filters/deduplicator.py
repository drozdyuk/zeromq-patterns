import zmq
import time
import sys

def deduplicator(port, next_filter_port):
    msgs = set()

    context = zmq.Context()
    socket = context.socket(zmq.PULL)
    socket.bind("tcp://*:%s" % port)

    next_filter = context.socket(zmq.PUSH)
    next_filter.connect("tcp://127.0.0.1:%s" % next_filter_port)

    for reqnum in range(15):
        # Wait for next request from client
        message = socket.recv()
        if message not in msgs:
            print("-- duplicator: %s" % message)
            msgs.add(message)
            next_filter.send(message)
        else:
            print("-- duplicator: duplicate. ignoring...")
