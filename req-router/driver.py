import zmq
from multiprocessing import Process
import helpers as h

def req(connect_port):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect(h.addr(connect_port))

    for i in range(1,6):
        socket.send("Hello %s" % i)
        msg = socket.recv()
        print("REQ received: %s" % msg)

def router(bind_port):
    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.bind(h.addr(bind_port))

    for i in range(1,6):
        ident, _, msg = socket.recv_multipart()
        print("ROUTER received : %s" % msg)
        reply = "%s World" % i
        socket.send_multipart([ident, b"", reply])



if __name__ == '__main__':
    port = h.get_port()
    Process(target=req, args=(port,)).start()
    Process(target=router, args=(port,)).start()
