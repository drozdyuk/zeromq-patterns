import zmq
from multiprocessing import Process
import helpers as h

def req(connect_port):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect(h.addr(connect_port))

    for i in range(1,6):
        socket.send("Hello %s" % i)
        msg = socket.recv()
        print("REQ received: %s" % msg)

def rep(bind_port):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind(h.addr(bind_port))

    for i in range(1,6):
        msg = socket.recv()
        print("REC received: %s" % msg)
        socket.send("%s World" % i)



if __name__ == '__main__':
    port = h.get_port()
    Process(target=req, args=(port,)).start()
    Process(target=rep, args=(port,)).start()
