"""
Assign work to workers as they are done. Not in round robin fashion.
Employees - REQ
Boss      - ROUTER
"""

import zmq
from multiprocessing import Process
import helpers as h
import time
import random
import names

NUM_EMPLOYEES = 3

def dealer_employee(connect_port):
    name = names.get_first_name()
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.connect(h.addr(connect_port))
    total = 0
    while(True):
        # Simulate REP envelope
        socket.send_multipart([b"", "%s done" % name])
        _, msg = socket.recv_multipart()
        if msg == 'GOHOME':
            print("EMPLOYEE(%s) going home after %s hours." % (name, total))
            break
        else:
            print("EMPLOYEE(%s) working: %s" % (name, msg))
            time.sleep(random.randint(1,2))
            total +=1

def router_boss(bind_port):
    employees = []

    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.bind(h.addr(bind_port))

    # We have a lot of work to do!
    num_tasks = 10 * NUM_EMPLOYEES
    for i in range(1, num_tasks + 1):
        ident, _, msg = socket.recv_multipart()

        print("BOSS received report: %s" % msg)
        socket.send_multipart([ident, b"", "Now do task #%s" % i])

    # All tasks done - let the employees go
    for i in range(NUM_EMPLOYEES):
        ident, _, msg = socket.recv_multipart()
        socket.send_multipart([ident, b"", "GOHOME"])


if __name__ == '__main__':
    port = h.get_port()
    for i in range(NUM_EMPLOYEES):
        Process(target=dealer_employee, args=(port,)).start()
    Process(target=router_boss, args=(port,)).start()
