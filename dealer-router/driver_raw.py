"""
Here we we send raw messages by NOT emulating REQ/REP envelope.
"""
import zmq
from multiprocessing import Process
import helpers as h

def dealer_raw(connect_port):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.connect(h.addr(connect_port))

    for i in range(1,6):
        socket.send("Hello %s" % i)
        msg = socket.recv()
        print("DEALER received: %s" % msg)

def router_raw(bind_port):
    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.bind(h.addr(bind_port))

    for i in range(1,6):
        ident, msg = socket.recv_multipart()
        print("ROUTER received: %s" % msg)
        reply = "%s World" % i
        socket.send_multipart([ident, reply])



if __name__ == '__main__':
    port = h.get_port()
    Process(target=dealer_raw, args=(port,)).start()
    Process(target=router_raw, args=(port,)).start()
