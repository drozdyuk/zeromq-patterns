"""
We emulate REQ/REP envelop here - otherwise
`b""` and `_` are not required.
"""
import zmq
from multiprocessing import Process
import helpers as h

def dealer(connect_port):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.connect(h.addr(connect_port))

    for i in range(1,6):

        socket.send_multipart([b"", "Hello %s" % i])
        _, msg = socket.recv_multipart()
        print("DEALER received: %s" % msg)

def router(bind_port):
    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.bind(h.addr(bind_port))

    for i in range(1,6):
        ident, _, msg = socket.recv_multipart()
        print("ROUTER received: %s" % msg)
        reply ="%s World" % i
        socket.send_multipart([ident, b"", reply])



if __name__ == '__main__':
    port = h.get_port()
    Process(target=dealer, args=(port,)).start()
    Process(target=router, args=(port,)).start()
