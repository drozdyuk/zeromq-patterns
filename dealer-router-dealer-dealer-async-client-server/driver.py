"""
Client talks async to server.
Server delegates to workers, and then routes back to client.
Clients (dealer) -> Server (router-server-dealer) -> Workers [Thread] (dealer)

http://zguide.zeromq.org/py:all#The-Asynchronous-Client-Server-Pattern
"""
import zmq
from multiprocessing import Process
import helpers as h
import time
import random

NBR_CLIENTS = 2
NBR_WORKERS = 2

def client_dealer(connect_port):
    """Generates one request per second"""
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.connect(h.addr(connect_port))
    name = h.name("C")
    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)

    request_nbr = 1
    while(True):
    # Wait at least 1 second before sending next request
    # But accept all messages in the meantime
        for centitick in range(1, 100):
            socks = dict(poller.poll(10)) # 10 msec at a time
            if socket in socks:
                # Got a reply
                reply = socket.recv()
                print("%s got reply: %s" % (name, reply))

        socket.send("Request from %s #%s" % (name, request_nbr))
        request_nbr += 1

def router_server_dealer(bind_port):
    backend_port = int(bind_port) + 1
    # Start workers
    for i in range(NBR_WORKERS):
        Process(target=dealer_worker, args=(backend_port,)).start()

    context = zmq.Context()

    backend = context.socket(zmq.DEALER)
    backend.bind(h.addr(backend_port))

    frontend = context.socket(zmq.ROUTER)
    frontend.bind(h.addr(bind_port))

    zmq.proxy(frontend, backend) # <---- This tiny call is equivalent to BELOW:
    # # Listen to either work done or client requests
    # poller = zmq.Poller()
    # poller.register(backend, zmq.POLLIN)
    # poller.register(frontend, zmq.POLLIN)
    #
    # while(True):
    #     socks = dict(poller.poll())
    #     if frontend in socks:
    #         # Handle new work
    #         client_ident, work = frontend.recv_multipart()
    #         backend.send_multipart([client_ident, work])
    #     if backend in socks:
    #         # Handle reply
    #         client_ident, reply = backend.recv_multipart()
    #         frontend.send_multipart([client_ident, reply])

def dealer_worker(connect_port):
    """ Listens for work requests and generates
    random number of replies as a response.
    Started by the server. """
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.connect(h.addr(connect_port))
    name = h.name("W")

    while(True):
        # Get work if any
        envelope, work = socket.recv_multipart()
        print("%s received work: %s" % (name, work))
        time.sleep(1)
        nbr_parts = random.randint(1,5)
        for i in range(1, nbr_parts + 1):
            socket.send_multipart([envelope, "Result [%s/%s]" % (i, nbr_parts)])

if __name__ == '__main__':
    port = h.get_port()
    for i in range(NBR_CLIENTS):
        Process(target=client_dealer, args=(port,)).start()
    Process(target=router_server_dealer, args=(port,)).start()
