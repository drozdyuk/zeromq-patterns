def addr(port):
    """Return local address string from a given port:
    i.e. tcp://127.0.0.1:1233
    """
    return "tcp://127.0.0.1:%s" % port

def get_port():
    """
    Get port from the user
    """
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('port')
    args = parser.parse_args()
    return args.port

def name(prefix):
    import uuid
    """Generate a random name with a give prefix"""
    return "%s-%s" % (prefix, str(uuid.uuid4())[:4])
