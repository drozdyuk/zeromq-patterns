"""
Example of how setting ZMQ_ROUTER_MANDATORY flag on a router socket
throws an error if you try to send a message to the router
without including a correct identity (i.e. identity that
has address associated with it)
"""
import zmq
from multiprocessing import Process
import helpers as h

def req(connect_port):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    # Explicitly set identiy - for the sake of example
    socket.set(zmq.IDENTITY, 'REQ')
    socket.connect(h.addr(connect_port))

    socket.send("Correct")
    msg = socket.recv()
    print("REQ: Recieved %s" % msg)
    socket.send("Wrong")
    msg = socket.recv()
    print("REQ: Recieved %s" % msg)


def router(bind_port):
    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.set(zmq.ROUTER_MANDATORY, True)
    socket.bind(h.addr(bind_port))

    for i in range(2):
        ident, _, msg = socket.recv_multipart()
        assert ident == 'REQ' # we set this in the REQ socket options
        if msg == 'Correct':
            # Reply correctly
            print("ROUTER: Received %s" % msg)
            socket.send_multipart(['REQ', b"", "Indeed!"])
        else:
            # Don't include the correct address!
            print("ROUTER: Received %s" % msg)
            try:
                socket.send_multipart(["JOE", b"", "Indeed!"])
                assert False
            except zmq.ZMQError as e:
                # All good here, go ahead and send the correct one:
                socket.send_multipart(['REQ', b"", "Sigh, ok!"])


if __name__ == '__main__':
    port = h.get_port()
    Process(target=req, args=(port,)).start()
    Process(target=router, args=(port,)).start()
