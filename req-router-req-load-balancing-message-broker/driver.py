import zmq
from multiprocessing import Process
import helpers as h
import time
import names
import uuid
NUM_WORKERS = 6
NUM_CLIENTS = 10

def client_req(connect_port):
    name = "c%s-%s" % (names.get_first_name(), str(uuid.uuid4())[:6])
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.set(zmq.IDENTITY, name)
    socket.connect(h.addr(connect_port))
    # Just do one request!
    socket.send("Work requested by %s" % name)
    reply = socket.recv()
    print("%s received reply: %s" % (name, reply))

def router_proxy_router(clients_bind_port, workers_bind_port):
    clients = NUM_CLIENTS
    workers = []
    context = zmq.Context()
    client = context.socket(zmq.ROUTER)
    client.bind(h.addr(clients_bind_port))

    worker = context.socket(zmq.ROUTER)
    worker.bind(h.addr(workers_bind_port))

    poller = zmq.Poller()
    poller.register(worker, zmq.POLLIN)

    while True:
        # Always check worker msgs first
        sockets = dict(poller.poll())

        if worker in sockets:
            msg = worker.recv_multipart()
            worker_id, delim, client_id = msg[:3]
            assert not delim

            # We got a free worker!
            if not workers: # if client was de-registered:
                poller.register(client, zmq.POLLIN)
            workers.append(worker_id)
            assert (len(workers) <= NUM_WORKERS)
            if client_id != req_worker.READY:
                # Send the client the results
                delim, reply = msg[3:]
                assert not delim
                print("r:Sending reply to client... %s" % client_id)
                client.send_multipart([client_id, b"", reply])
                clients-=1
                if clients <= 0:
                    print("r:No more clients left. Shutting down broker.")
                    break

        if client in sockets:
            client_id, delim, work = client.recv_multipart()
            assert not delim
            worker_id = workers.pop(0)
            if not workers: # no more workers left - stop getting work:
                poller.unregister(client)

            worker.send_multipart([worker_id, b"", client_id, b"", work])

    client.close()
    worker.close()
    context.term()

def req_worker(connect_port):
    name = "w%s-%s" %(names.get_first_name(), str(uuid.uuid4())[:6])
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.set(zmq.IDENTITY, name)
    socket.connect(h.addr(connect_port))
    socket.send(req_worker.READY)

    # Shut-down worker if there is no activity! --A.D.
    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)

    while(True):
        if socket in dict(poller.poll(5000)):
            m = socket.recv_multipart()
            client_id, delim, msg = m
            assert not delim
            print("%s doing work: %s" % (name, msg))
            time.sleep(1)
            socket.send_multipart([client_id, b"", "%s done." % name])
        else:
            print("%s Shutting-Down. No work coming in!" % name)
            break
req_worker.READY = "READY"


if __name__ == '__main__':
    port = int(h.get_port())
    for i in range(NUM_CLIENTS):
        Process(target=client_req, args=(port,)).start()
    for i in range(NUM_WORKERS):
        Process(target=req_worker, args=(port+1,)).start()
    Process(target=router_proxy_router, args=(port, port+1)).start()
