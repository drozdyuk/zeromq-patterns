"""
TODO: NOT SURE HOW TO MAKE THIS WORK!
See TODOs in code for problems.
"""
import zmq
from multiprocessing import Process
import helpers as h

def router1(connect_port, announce_port):
    context = zmq.Context()

    # Wait for ident announcement from the "server"
    intro = context.socket(zmq.ROUTER)
    intro.bind(h.addr(announce_port))
    print("1: Waiting for intro...")
    ident, _, msg = intro.recv_multipart()
    print("1: Introdec %s" % ident)

    socket = context.socket(zmq.ROUTER)
    socket.connect(h.addr(connect_port))

    for i in range(1,6):
        # TODO: For some reason this doesn't send msg
        # (because socket router doesn't have an ident
        # of 'router2_ident' associated with the address)
        socket.send_multipart([ident, b"", "Hello %s" % i])
        print("route1: Sent multipart")
        ident, msg = socket.recv_multipart()
        print("REQ received: %s" % msg)

def router2(bind_port, announce_port):
    context = zmq.Context()

    print("2: Announcing")
    announce = context.socket(zmq.REQ)
    announce.set(zmq.IDENTITY, "router2_ident")
    announce.connect(h.addr(announce_port))
    announce.send("HELLO")
    print("2: Announced.")

    socket = context.socket(zmq.ROUTER)
    socket.bind(h.addr(bind_port))
    print("Router2 bound")
    for i in range(1,6):
        print("ROUTER 2 in loop")
        ident, _, msg = socket.recv_multipart()
        print("REC received: %s" % msg)
        socket.send_multipart([ident, "%s World" % i])



if __name__ == '__main__':
    port = h.get_port()
    Process(target=router1, args=(port, port+1)).start()
    Process(target=router2, args=(port, port+1)).start()
