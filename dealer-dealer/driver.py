import zmq
from multiprocessing import Process
import helpers as h

def dealer1(connect_port):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.connect(h.addr(connect_port))

    for i in range(1,6):
        socket.send("Hello %s" % i)

    for i in range(1,6):
        msg = socket.recv()
        print("DEALER1 received: %s" % msg)

def dealer2(bind_port):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.bind(h.addr(bind_port))

    for i in range(1,6):
        msg = socket.recv()
        print("DEALER2 received: %s" % msg)

    import time
    time.sleep(1)

    for i in range(1,6):
        socket.send("%s World" % i)



if __name__ == '__main__':
    port = h.get_port()
    Process(target=dealer1, args=(port,)).start()
    Process(target=dealer2, args=(port,)).start()
